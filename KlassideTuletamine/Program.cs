﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine  // properties ja seal on DEFAULT NAMESPACE
{//siia ma võin ka kirjtuada klasse, enumme ja structe
    static  class Program
    {
        //static string Filename;  // saab kasutada kogu programmi classi sees
        //int miskiArv = 7; // seda ei saa mitte staticus kasutada
        static void Main(string[] args)
        {
            Loom loom1 = new Loom("krokodill");
            Console.WriteLine(loom1.Liik);

            Loom loom2 = new Loom("ämblik");
            Console.WriteLine(loom2.Liik);
            //loom1.Liik = "Krokodill";
            Loom loom3 = new Loom();
            Console.WriteLine(loom3.Liik);
            Koduloom koduloom1 = new Koduloom("Albert", "prussakas");
            Console.WriteLine(koduloom1);



            Kass kiisu = new Kass { Nimi = "Garfield", Tõug = "Gaarfiildi tõug" };
            Console.WriteLine(kiisu);
            kiisu.Silita();

            foreach (var l in Loom.Loomaaed)
            {
                if (l is ISöödav ls) { ls.Söö(); }
                l.TeeHäält();
            }
            Koer k = new Koer { Nimi = "Polla" };
            k.TeeHäält();
            k.Söö();

            Sepik s = new Sepik();
            s.Söö();
            k.Söö();
            Lõuna(k);

            //Dictionary<Loom, string> imelikDictionary = new Dictionary<Loom, string>();

            var loomad = Loom.Loomaaed.ToArray();
            Array.Sort(loomad);
            foreach(var xx in Loom.Loomaaed)
            {
                // ((Kass)xx).Silita();  // casting annab vea, kui xx ei ole Kass
                (xx as Kass)?.Silita(); // as-operaator annab null kui xx ei ole Kass
                if (xx is Kass xxs) xxs.Silita(); // sama mis selle üleval
            }

        }
        public static void Lõuna(object x)
        {
            if (x is ISöödav xs) xs.Söö();
            else Console.WriteLine("täna jääme nälga");
        }
    }
   
    // siia structid, enumid ja klassid
}
