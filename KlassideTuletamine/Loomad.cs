﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    // siia saan kirjutada klasse ja enumme ja structe
    // klasside, struktide,enummide järjekord namepsaces ei ole oluline
    interface ISöödav   // interface hakkab kahe suure tähega
    {
        void Söö();
    }

    class Sepik : ISöödav
    {
        public void Söö()
        {
            Console.WriteLine("keegi nosib sepikut");
        }
    }
    class Loom : IComparable
    {
        public static List<Loom> Loomaaed = new List<Loom>();
        public readonly string Liik; // looma liiki peale "loomist" muuta ei saa

        public Loom() { } // kui seda pole, siis ei saagi kodulooma teha

        public Loom(string liik)        // ilma liigita looma ei saa teha
        {
            Liik = liik;
            Loomaaed.Add(this);
        }
        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält");   // virtual tähendab, et tuletatud klassis on võimalik see meetod ära muuta
        //ümber defineerid ehk overraidida

        public override string ToString() => $"loom liigist{Liik}";
        //public override string ToString() { return $"loom liigist{Liik}"; }  
        public virtual void Pigista() { }

        int IComparable.CompareTo(object obj)
        {
            if (obj is Loom l) return this.Liik.CompareTo(l.Liik);
            else return -1;
        }
    }
    class Koduloom : Loom   /// näitab et koduloom on ka loom
    {
        public string Nimi;  /// sellepoolest erineb loomast, et tal on nimi


        //public Koduloom(string nimi, string liik) => (Nimi, Liik) = (nimi, liik);<<<<<  Sama asi mis üleval
        public Koduloom(string nimi, string liik) : base(liik)
        {     // Kodulooma tegemisel peab enne looma tegema ja talle liigi ütlema
              // base(liik)on p;;rdumine baasklassi(Loom) konstruktori poole
            Nimi = nimi;
            //    
        }
        public override string ToString() => $"{Liik} {Nimi}"; // teine asi mis on kirjeldatud teistmoodi
        public override void TeeHäält() => Console.WriteLine($"väike {Nimi} möriseb mahedasti");


    }
    class Kass : Koduloom
    {
        public string Tõug;
        private bool Tuju = false;
        public void SikutaSabast() => Tuju = false;
        public void Silita() => Tuju = true;
        public Kass() : base("nimeta", "kass") { }
        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kass {Nimi} lööb nurru");
            else Console.WriteLine($"{Nimi} kräunub");
        }
    }
    class Koer : Koduloom , ISöödav
    {
        public string KoeraTõug; // see ei ole sama asi, mis kassi tõug
        public Koer() : base ("nimeta", "koer") { }

        public void Söö()
        {
            Console.WriteLine($"{Nimi} pannakse nahka");
        }

        public override void TeeHäält()
        {
            Console.WriteLine($"{Nimi} haugub usinasti");
        }
    }
}
