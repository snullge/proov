﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace extensionidJaTorud
{
    static class Program
    {
        static class E
        {
            public static Paar Liida(Paar t1, Paar t2) => new Paar(t1.a + t2.a, t1.b + t2.b);
        }
        public static Paar Korruta(this Paar t1, Paar t2) => new Paar(t1.a * t2.a, t1.b * t2.b);
        public static string ToProper(this string s)
        {
            return s == "" ? "" :
                s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();
        }
        public static string Join<T>(this IEnumerable<T> mass, string sep)
            => "{" + string.Join(sep, mass) + "}";
        public static IEnumerable<int> Paaris(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 == 0) yield return x;
        }
        public static IEnumerable<int> Paaritud(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 != 0) yield return x;
        }
        public static IEnumerable<T> Millised<T>(this IEnumerable<T> mass, Func<T, bool> f)
        {
            foreach (var x in mass)
                if (f(x)) yield return x;
            
        }
        public static IEnumerable<T> Võta<T>(this IEnumerable<T> mass, int mitu)  //TAKE
        {
            foreach (var x in mass)
                if (mitu-- > 0) yield return x;
        }
        public static IEnumerable<T> Jäta<T>(this IEnumerable<T> mass, int mitu)  // SKIP
        {
            foreach (var x in mass)
                if (mitu--< 0) yield return x;
        }
        static void Main(string[] args)
        {
            #region paarid
            //Console.WriteLine("Anna oma nimi: ");
            //Console.WriteLine(Console.ReadLine().ToProper());



            //Paar t = new Paar(3, 4);
            //Console.WriteLine(t);
            //Paar teine = new Paar(5, 3);
            //Console.WriteLine(t.Liida(teine));
            //Console.WriteLine(Paar.Liida(t,teine));

            //Console.WriteLine(Korruta(t, teine));
            //Console.WriteLine(t.Korruta(teine)); 
            #endregion

            int[] arvud = { 1, 2, 7, 3, 4, 2, 9, 6, 2 };
            Console.WriteLine(arvud.Join(","));
            //foreach (var x in arvud) Console.WriteLine(x);
            foreach(var x in arvud.Paaris())
                Console.WriteLine(x);
            Console.WriteLine(arvud
                .Paaris()
                .Join(",")
                .Skip(3)
                );
            Console.WriteLine(arvud.Paaritud().Join(","));
            Console.WriteLine(arvud.Millised(x => x > 5).Join(","));
            Console.WriteLine(arvud.Where(x => x > 5).Join(","));           //// WHERE FUNCTSIOON
            #region LAMBDA FUNC
            Func<int, int> ruut = x => x * x;  // func = parameetri tüüp, tulemuse tüüp LAMBDA FUNKTSIOON
            Func<int, int> kuup = x => x * x * x;
            Func<int, int, int> liida = (x, y) => x + y;
            Console.WriteLine(ruut(4));
            Console.WriteLine(kuup(4));
            #endregion
            Console.WriteLine(
                arvud
                .Select(x => x*x)
                .Join(",")
                
                );
        }
    }
    class Paar
    {
        public int a;
        public int b;
        
        public Paar(int a ,int b) => (this.a, this.b) = (a,b);
        public override string ToString() => ($"{a}, {b}");

        public Paar Liida(Paar t) => new Paar(this.a + t.a, this.b + t.b);
        
    }
}
