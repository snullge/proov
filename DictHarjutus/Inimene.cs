﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictHarjutus
{
    enum Sugu { Naine, Mees}
    class Inimene
    {
        //staatilised asjad
        public static int InimesteArv { get; private set; } = 0; /// inimeste loendur
        static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        //public Dictionary<string, Inimene>.ValueCollection Inimesed => _Inimesed.Values;
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;      //public dic


        // objekti asjad
        // väljad ja propertid
        public readonly int Nr = ++InimesteArv;
        public string Eesnimi { get => _Eesnimi; set => _Eesnimi = ToProper(value) ; } 
        private string _Eesnimi;
        public string Perenimi { get => _Perenimi; set => _Perenimi = ToProper(value) ; } 
        private string _Perenimi { get; set; }
        public string Nimi => Eesnimi + " " + Perenimi;

        public string Isikukood { get; private set; }
        
        public static Inimene New(string isikukood)
        {
            return _Inimesed.ContainsKey(isikukood)
                ? _Inimesed[isikukood]
                : new Inimene(isikukood);
        }

        
        // konstruktorid

        public Inimene(string isikukood)
        {
            Isikukood = isikukood;
            if (!_Inimesed.ContainsKey(isikukood)) _Inimesed.Add(isikukood, this);
        }

        // funktsioonid ja readonly propertid
        public Sugu Sugu => (Sugu)(Isikukood[0] % 2);

        public DateTime Sünniaeg =>
            DateTime.Parse(
                (
                Isikukood[0] < '3' ? "18" :
                Isikukood[0] < '5' ? "19" :
                Isikukood[0] < '7' ? "20" :
                "21"
                )
                + Isikukood.Substring(1, 2) + 
                "/" + Isikukood.Substring(3, 2) + 
                "/" + Isikukood.Substring(5, 2)             
                );



        // meetodid 
        static string ToProper(string s) 
            => s =="" ? "":
            s.Substring(0,1).ToUpper() + s.Substring(1).ToLower();
        //overraidid
        public override string ToString() => $" {Nr}. {Sugu} {Nimi} on sÜndinud {Sünniaeg:dd.MM.yy}";
    }
}
