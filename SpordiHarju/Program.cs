﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordiHarju
{
    static class E {
        public static IEnumerable<string> LoeRead(this string filename)
        {
            return File.ReadAllLines(filename);
        }
        public static string Join<T>(this IEnumerable<T> mass, string sep)
           => "{" + string.Join(sep, mass) + "}";
        public static void MyForEach<T>(this IEnumerable<T> mass, Action<T> a)
        {
            foreach (var x in mass) a(x);
        }


        class Program
        {
            static void Main(string[] args)
            {
                var spp = @"C:\Users\rpyym\source\repos\TeineTeisiP\SpordiHarju\sport.txt"
                .LoeRead()
                .Skip(1)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Where(x => x.Length > 2)
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                .ToArray()
                
                
                ;



                var q1 = from x in spp
                         where x.Distants == 100
                         orderby x.Nimi
                         select x.Nimi;
                Console.WriteLine(" 100 meetrit jooksid" + q1.Join(", "));

                var q2 = spp.Where(x => x.Distants == 100).OrderBy(x => x.Nimi).Select(x => x.Nimi);

                var distantsiKiireimad =
                    spp
                    .ToLookup(x => x.Distants)
                    .Select(x => new { Distants = x.Key, Kiireim = x.OrderByDescending(y => y.Distants * 1.0 / y.Aeg).First().Nimi });

                Console.WriteLine("Distantside kiireimad olid:");
                distantsiKiireimad
                    //.ToList() // teen ta listiks, kuna listil on tore asi ForEach
                    // teistel ei ole, aga ma kohe teen - MyForEach
                    //.ForEach(x => Console.WriteLine(x));
                    .MyForEach(x => Console.WriteLine(x));





            }
        }
    }
}
