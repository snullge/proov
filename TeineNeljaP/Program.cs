﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineNeljaP
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("anna yks arv: ");
                int arv = int.Parse(Console.ReadLine());
                Console.Write("anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());
                if (teine != 0)
                    Console.WriteLine(arv / teine);
                else
                {
                    Console.WriteLine("nulliga mina ei jaga");
                    throw new Exception("ise oled null");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("midagi juhtus");
                Console.WriteLine(e.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("pulmalaud tuleb ikka kinni maksta");
            }
            Console.WriteLine("lõppeb ikka hästi");

        }
    }
}
