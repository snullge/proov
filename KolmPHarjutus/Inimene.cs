﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolmPHarjutus
{
    class Inimene
    {
        public string Nimi;
        public string Isikukood;
        private string inimene;

        public Inimene() { }

        public Inimene(string inimene)
        {
            this.inimene = inimene;
        }

        public Inimene(string nimi, string isikukood)
        {
            Nimi = nimi;
            Isikukood = isikukood;

        }
    }
    class Õpilane : Inimene
    {
        
        public string Klass;
        public Õpilane(string klass, string inimene) : base(inimene)
        {
            
            Klass = klass;

        }
    }
    class Õpetaja : Inimene
    {
        public string Aine;
        public string Klass;

        public Õpetaja(string aine, string klass, string inimene) : base(inimene)
        {
            Klass = klass;
            Aine = aine;
        }
    }
}
