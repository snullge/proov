﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KlassiMega
{
    class Program
    {
        static void Main(string[] args)
        {
            var õpilaneFile = @"C:\Users\rpyym\source\repos\TeineTeisiP\TeineReede\Õpilased (1).txt";
            var õpetajaFile = @"C:\Users\rpyym\source\repos\TeineTeisiP\TeineReede\Õpetajad (1).txt";
            var ainedFile = @"C:\Users\rpyym\source\repos\TeineTeisiP\TeineReede\KlassJaAine.txt";
            var hindedFile = @"C:\Users\rpyym\source\repos\TeineTeisiP\TeineReede\Hinded.txt";
            // aslustame õpilastega - IK, Nimi, klass
            var õpilased = File.ReadAllLines(õpilaneFile)
                .Select(x => x.Replace(", ", ",").Split(',')) // loetud read massiiviks
                .Select(x => new { Tüüp = "õpilane", IK = x[0], Nimi = x[1], Klass = x[2], Aine = "" }) /// PEAB OLEMA AINE KA , muidu on KATKI
                .ToList() //saveb listina hea pärast kasutada
                ;
            //foreach (var x in õpilased) Console.WriteLine(x);
            var õppetajad = File.ReadAllLines(õpetajaFile)
                .Select(x => (x + ",").Replace(", ", ",").Split(',')) // loetud read massiiviks
                .Select(x => new { Tüüp = "õpetaja", IK = x[0], Nimi = x[1], Klass = x[3], Aine = x[2] })
                .ToList() //
            ;
            //foreach (var x in õppetajad) Console.WriteLine(x);
            var koolipere = õpilased.Union(õppetajad);
            ;

            foreach (var x in koolipere) Console.WriteLine(x);

            var hinded
                = File.ReadAllLines(hindedFile)
                .Select(x => x.Split(','))
                .Select(x => new { ÕpsiK = x[0], ÕpilaneIK = x[1], Aine = x[2], Hinne = int.Parse(x[3]) })
                .ToLookup(x => x.ÕpilaneIK)  //// 
                ;
            //if (false)
            foreach (var x in hinded)
            {
                Console.WriteLine($"õpilase {x.Key} hinded:");
                foreach (var y in x)
                    Console.WriteLine($"\t{y.Aine} {y.Hinne}");
            }

            foreach (
                var x in õpilased
                .Select(x => new { x.Nimi, x.Klass,
                    Keskmine = hinded[x.IK]
                .DefaultIfEmpty()
                .Average(y => y?.Hinne ?? 0) }

                )
                .OrderByDescending(x => x.Keskmine)  //// reastab keskmise järgi
                .ToLookup(x => x.Klass) /// klasside kaupa
                .Select(x => new { Klass = x.Key, Parim = x.OrderByDescending(y => y.Keskmine).First() }) /// võtab klassi nime, võtab klassi õpilased, sorteerib keskmise järgi ja sealt esimene
            ) Console.WriteLine(x);
            
            


            
            //// sÜnnipäev = kellel järgmisel nädalal
            //var sünnipäevad = koolipere
            //    .Select(x => new { x.Nimi, Kuu = x.IK.Substring(3, 2), Päev = x.IK.Substring(5, 2) })
            //    .Select(x => new { x.Nimi, Sünnipäev = new DateTime(DateTime.Today.Year, int.Parse(x.Kuu), int.Parse(x.Päev)) })
            //    .Select(x => new { x.Nimi, Sünnipäev = x.Sünnipäev < DateTime.Today ? x.Sünnipäev.AddYears(1) : x.Sünnipäev })
            //    .ToList();

            //DateTime uusNädal = DateTime.Today.AddDays(DateTime.Now.DayOfWeek ==
            //    DayOfWeek.Sunday ? 1 : (8 - (int)DateTime.Now.DayOfWeek));
            //Console.WriteLine($"uus nädal algab{uusNädal} ja lõppeb {uusNädal.AddDays(7)}");
            //foreach (var s in sünnipäevad
            //    .OrderBy(x => x.Sünnipäev)
            //    .Take(5)
            //    //.Where(x=> x.Sünnipäev > DateTime.Today && x.Sünnipäev <= DateTime.Today.AddDays(7)) // näitab mitu sünnipäeva on järgmisel nädalal <<<adddaysi
            //    .Where(x => x.Sünnipäev >= uusNädal && x.Sünnipäev < uusNädal.AddDays(7))
            //    )Console.WriteLine(s);
        }
    }
}
