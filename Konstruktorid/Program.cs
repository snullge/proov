﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktorid
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene("38709200376") { Nimi = "Henn", Vanus = 64 };
            Inimene ants = new Inimene("38009200376") { Nimi = "Ants", Vanus = 40 };
            Inimene peeter = new Inimene("39009200376") { Nimi = "Peeter", Vanus = 30 };
            Inimene jumbu = new Inimene { Nimi = "Jumbu" };
            new Inimene("Joosep", "50101010000"); /// kolmas konstruktor
            Console.WriteLine(henn);
            Console.WriteLine(ants);
            Console.WriteLine(peeter);
            Console.WriteLine(jumbu);
            //foreach (var x in Inimene.Inimesed) Console.WriteLine(x);
        }
    }
}
