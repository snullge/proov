﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktorid
{
    class Inimene
    {
        static List<Inimene> _Inimesed = new List<Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.AsEnumerable();

        public static int InimesteArv { get; private set; } = 0;  // see on static property

        public readonly string Isikukood;

        // see siin on konstruktor, kui selle teen siis ilma isikukoodita inimest ei saa enam luua
        public Inimene(string isikukood)
        {
            Isikukood = isikukood;
            _Inimesed.Add(this);

        }

        public Inimene() : this("00000000000") { }

        public Inimene(string nimi, string isikukood) : this(isikukood) { Nimi = nimi; }
        public Inimene(string nimi, string isikukood, int vanus) : this(isikukood) => (Nimi, Vanus) = (nimi, vanus);
        //{
        //    Nimi = nimi;
        //    Vanus = vanus;
        //}
        


        //public static int InimesteArv <<<<<<<<<<<<<<<<<< see on static property
        //{
        //    get => _InimesteArv;
        //    private set => _InimesteArv = value;
        //}                     


        // objekti väljad (instantsi e eksemplari omad)
        public  readonly int Nr = ++InimesteArv;   // selle mutuja väärtust saab ainult init avaldisega
        public string Nimi;
        public int Vanus;

        // see on vajalik, et inimest saaks stringiks teisendada(ConsoleWriteLine, $-string jt)
        // sellise rea vÕiks igalge klassile(structile) lisada
        // muidu teeks ToString() tast "Konstruktorid.Inimene"

        public override string ToString() 
            => $"{Nr}.{Nimi} vanus: {Vanus} (kokku {InimesteArv} {(InimesteArv==1?"inimene" : "inimest")} inimest)";
    }
}
