﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VahvaTegelaneJson
{
    class Program
    {
        static void Main(string[] args)
        {
            if (false)
            {
                Inimene[] hennud =
                    {
                new Inimene { Nimi = "Henn Sarv", Vanus = 64 },
                new Inimene { Nimi = "Toomas Linnupoeg", Vanus = 27 },
                new Inimene { Nimi = "Peeter Suur", Vanus = 28 },
                new Inimene { Nimi = "Ants Saunamees", Vanus = 40 },
                new Inimene { Nimi = "Tiit Tublimees", Vanus = 80 },
                };

                List<string> read = new List<string>
                {"Nimi,Vanus" };

            
                foreach (var x in hennud) read.Add(x.Salvestamiseks());

                File.WriteAllLines(@"..\..\hennud.txt", read);


                var read = File.ReadAllLines(@"..\.. \hennud.txt");
                List<Inimene> uued = new List<Inimene>();
            for (int i = 1; i < read.Length; i++)

                {
                    uued.Add(new Inimene
                    {
                        Nimi = read[i].Split(',')[0],
                        Vanus = int.Parse(read[i].Split(',')[1])
                    }
                     );

                
                }
            }
        }
    }

    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        public string Salvestamiseks()
            => $"{Nimi},{Vanus}";

    }

}
