﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineTeisiP
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i;  // muutuja
            i = new Inimene(); //sellega tekivad string nimi ja int vanus classis inimene muutujad
            i = new Inimene { Nimi = "Henn", Kaasa = new Inimene { Nimi = "Maris" } };
            Inimene i2; // tekib teine komplekt classis inimene
            i2 = new Inimene();
            Console.WriteLine(i);
            Console.WriteLine(i.Kaasa?.Nimi);  // ? pöördu selle olematu asja nime poole

            
            
        
        }
    }
    struct Miski
    {
        public static int AsjadeArv;
        public string Nimetus;
        public int Väärtus;
    }
}
