﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineTeisiP
{
    class Inimene
    {
        public static int InimesteArv = 0;  // tekib siis kui esimest korda mainitakse mingit väärtust antud klassis
        public string Nimi; // vaikimisi e default väärtused
        public int Vanus;   // vaikimisi e default väärtused
        public Inimene Kaasa; // classidel on väärtus kandiline null 

        public override string ToString() => $"{Nimi} vanusega {Vanus}";
        //
    }
}
