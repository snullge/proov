﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VeelSpordiHarju
{
    class Program
    {
        static void Main(string[] args)
        {
            var jooksuList = @"C:\Users\rpyym\source\repos\TeineTeisiP\VeelSpordiHarju\sport.txt";

            var jooks = File.ReadAllLines(jooksuList)
                .Skip(1)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg})
                .ToList()
                ;
            var jooksjad = jooks.ToLookup(x => x.Nimi);
            Console.WriteLine("\nKõige kiirem jooksja\n");
            Console.WriteLine(
                jooks.OrderByDescending(x => x.Kiirus).FirstOrDefault()?.Nimi ?? "puudub"
                );
            Console.WriteLine(
                string.Join("\n",
                jooks
                .GroupBy(x=> x.Kiirus).OrderBy(x=> x.Key).LastOrDefault()
                )
                );
            // kes on kõige stabiilsem
            Console.WriteLine(
                string.Join("\n",
            jooksjad
                .Where(x => x.Count() > 1)
                .Select(x => new { Nimi = x.Key, Min = x.Min(y => y.Kiirus), Max = x.Max(y => y.Kiirus) })
                .Select(x => new { x.Nimi, Vahe = x.Max - x.Min, x.Max,x.Min })
                .OrderBy(x => x.Vahe)
                .FirstOrDefault()
                )
               ) ;

                
            

        }
        
        
    }
}
