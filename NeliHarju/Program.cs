﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NeliHarju
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedSet<Inimene> koolipere = new SortedSet<Inimene>
            {
                //new Õpilane {Nimi = "henn", Klass = "1A", Isikukood = "35555555555"}-

            };
                
            
            string õpilasteFail = @"C:\Users\rpyym\Õpilased.txt";
            string õpsideFail = @"C:\Users\rpyym\Õpetajad.txt";
            foreach (var rida in File.ReadAllLines(õpilasteFail))
            {
                var osad = rida.Replace(", ", ",").Split(',');
                koolipere.Add(new Õpilane
                {
                    Isikukood = osad[0],
                    Nimi = osad[1],
                    Klass = osad[2]
                });

            }
            
            
            
            foreach (var rida in File.ReadAllLines(õpsideFail))
            {
                    var osad = (rida+",,").Replace(", ", ",").Split(',');
                    koolipere.Add(new Õpetaja
                    {
                        Isikukood = osad[0],
                        Nimi = osad[1],
                        Aine = osad[2],
                        Klass = osad [3]
                    });
            }
            foreach (var x in koolipere) Console.WriteLine(x);


        }
    }
    class Inimene : IComparable
    {
        public string Nimi { get; set; }
        public string Isikukood { get; set; }

        public int CompareTo(object obj)
        {
            return Nimi.CompareTo((obj as Inimene)?.Nimi??""); // esimene ? vaata ainult siis kui inimene, ?? kui ei olnud inimest keda vaadata, vaata tyhja stringi
        }

        public override string ToString() => $"{Nimi} (IK: {Isikukood})";
    }
    class Õpetaja : Inimene
    {
        public string Aine { get; set; }
        public string Klass { get; set; }
        public override string ToString() =>
            $"{Aine} õpetaja" +
            $"{Nimi} (IK: {Isikukood})"
            + (Klass == "" ? "" : $"({Klass} klassi juhataja)")
            ;
    }
    class Õpilane : Inimene
    {
        public string Klass { get; set; }
        public override string ToString() => $"{Klass} klassi õpilane {Nimi} (IK: {Isikukood})";

    }
}
