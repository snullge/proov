﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Json
{
    class Program
    {
        static void Main()
        {
            string filename = @"..\..\tulemus.json";
            string sisu = File.ReadAllText(filename);

            List <Inimene> inimesed = JsonConvert.DeserializeObject<List<Inimene>>(sisu);
            foreach(var x in inimesed)
                Console.WriteLine(x.Nimi);
        }
        static void Main(string[] args)
        {           
                Inimene[] hennud =
                    {
                new Inimene { Nimi = "Henn Sarv", Vanus = 64 },
                new Inimene { Nimi = "Toomas Linnupoeg", Vanus = 27 },
                new Inimene { Nimi = "Peeter Suur", Vanus = 28 },
                new Inimene { Nimi = "Ants Saunamees", Vanus = 40 },
                new Inimene { Nimi = "Tiit Tublimees", Vanus = 80 },
                };
            string nimikiri = JsonConvert.SerializeObject(hennud);
            //Console.WriteLine(nimikiri);
            string failiNimi = @"..\..\tulemus.json";
            File.WriteAllText(failiNimi, nimikiri);
        }
    }
    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }

    }
}
