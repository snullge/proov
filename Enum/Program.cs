﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum
{
    enum Mast { Risti, Ruutu, Ärtu, Poti }

    [Flags]
    enum Tunnus { Suur = 1, Puust = 2, Punane = 4, Kolisev = 8}
    class Program
    {
        static void Main(string[] args)
        {
            Mast m = Mast.Ruutu;
            m++;
            Console.WriteLine(m);

            int a = 9;
            m = (Mast)a;

            Tunnus t = (Tunnus)a;
            Console.WriteLine(t);

            t = Tunnus.Kolisev | Tunnus.Punane;
            if ((t & Tunnus.Punane) == Tunnus.Punane) Console.WriteLine("on punane");
            t |= Tunnus.Puust;
            Console.WriteLine(t);
            t ^= Tunnus.Punane;
            Console.WriteLine(t);


        }
    }
}
